<!DOCTYPE html>
<html lang="fr">
    <head>
        <link rel="icon" href="image/7BQx50y.ico" />
        <link rel="stylesheet" type="text/css" href="style_1.css">
        <meta charset="utf-8">
        <title>Balise &#60;a&#62; </title>

    </head>

    <body>
        <header>

            <nav>

                <ul class="navi">

                </ul>

            </nav>

        </header>

        <div class="titre">
            <br>
            <h1>La balise &#60;a&#62;</h1>
            <br>
            <p>La balise &#60;a&#62; signifie ancre ou anchor en anglais et definit l'intégration d'un hyperlien amenant à une autre page. C'est une balise de type inline.</p>
            <br>
            <p> download = Cet attribut indique si l'hyperlien est utilisé afin de télécharger une ressource <br>
                href = L'URL de la ressource liée.<br>
                hreflang = Cet attribut indique la langue utilisé pour la ressource liée.
            </p>
            <br>
            <p>
                <img src="image/Image_A.png" alt="balise a">
            </p>
            <br>
            <h4><a href="index_inline.php" >Sommaire inline</a></h4>
            <br>
            <h4><a href="index.php" >Retour au sommaire</a></h4>
            <br>
        </div>
    </body>
</html>
