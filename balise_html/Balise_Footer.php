<!DOCTYPE html>
<html lang="fr">
    <head>
        <link rel="icon" href="image/7BQx50y.ico" />
        <link rel="stylesheet" type="text/css" href="style_1.css">
        <meta charset="utf-8">
        <title>La balise &#60;footer&#62;</title>
    </head>
    <body>
        <header>
            <nav>
                <ul class="navi">
                </ul>
            </nav>
        </header>
        <div class="titre">
            <br>
            <h1>La balise &#60;footer&#62;</h1>
            <br>
            <p>La balise &#60;footer&#62; définit un pied de page d’un document.
                Il peut aussi définir le pied de page d’un élément &#60;article&#62;.
                Un élément &#60;footer&#62; contient généralement l’auteur du document, les informations de copyright, 
                des liens vers des conditions d’utilisation, informations de contact, etc..</p>
            <br>
            <p>L'attribut dir est un attribut universel permettant de définir le sens de lecture.</p>
            <br>
            <img class="img-footer" src="image/Footer.PNG" alt="footer">
            <br>
            <br>
            <h4><a href="index_corp.php" >Sommaire corps</a></h4>
            <br>
            <h4><a href="index.php" >Retour au sommaire</a></h4>
            <br>
        </div>
    </body>
</html>
