<!DOCTYPE html>
<html lang="fr">
    <head>
        <link rel="icon" href="image/7BQx50y.ico" />
        <link rel="stylesheet" type="text/css" href="style_1.css">
        <meta charset="utf-8">
        <title>La balise &#60;h1 à h6&#62;</title>
    </head>

    <body>
        <header>
            <nav>
                <ul class="navi">
                </ul>
            </nav>
        </header>
        <div class="titre">
            <br>             
            <h1>La balise &#60;h1 à h6&#62;</h1>
            <br>
            <p>Les balises &#60;h1 à h6&#62; sont des balises qui sont utilisées pour définir les titres de différents niveaux, 
                on peut en mettre jusqu'a 6 différentes, de &#60;h1&#62; a &#60;h6&#62;. La balise &#60;h1&#62; étant la plus importante, 
                c'est celle qui va désigner le gros titre du site web.
                C'est une balise de type in-line.</p>
            <br>
            <p>
                <img src="image/Balises_Exemple_Modifie.PNG" alt="balise h">
            </p>    
            <br>
            <h4><a href="index_block.php" >Sommaire block</a></h4>
            <br>
            <h4><a href="index.php" >Retour au sommaire</a></h4>
            <br>
        </div>
    </body>
</html>
