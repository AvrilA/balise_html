<!DOCTYPE html>
<html lang="fr">
    <head>
        <link rel="icon" href="image/7BQx50y.ico" />
        <link rel="stylesheet" type="text/css" href="style_1.css">
        <meta charset="utf-8">
        <title>La balise &#60;textarea&#62;</title>
    </head>

    <body>
        <header>
            <nav>
                <ul class="navi">
                </ul>
            </nav>
        </header>
        <div class="titre">
            <br>
            <h1>La balise &#60;textarea&#62;</h1>
            <br>
            <p>La balise &#60;textarea&#62; fait partie des éléments utilisés au sein des formulaires afin de récupérer des informations clients.
                C'est une balise de type block.</p>
            <p> 
                <br>
            <p> Cette balise utilise les attributs:<br/>
                -max length qui indique le nombre maximum de caractères pouvant être saisi.
                S'il n'est pas utilisé l'utilisateur peut saisir un nombre illimité de caractères.<br/>
                -minlength qui indique le nombre minimal de caractère que l'utilisateur doit saisir.<br/>
                -placeholder permet d'afficher un texte d'exemple dans un textarea qui disparaitra lorsque 
                l'utilisateur cliquera dans la zone de texte. </p>
            <br>
            <p>
                <img src="image/Textarea.PNG" alt="textarea">
            </p>    
            <br>

            <h4><a href="index_block.php" >Sommaire block</a></h4>
            <br>
            <h4><a href="index.php" >Retour au sommaire</a></h4>
            <br>
        </div>
    </body>

</html>
