<!DOCTYPE html>
<html lang="fr">

    <head>
        <link rel="icon" href="image/7BQx50y.ico" />
        <link rel="stylesheet" type="text/css" href="style_1.css">
        <meta charset="utf-8">
        <title>la balise &#60;body&#62;</title>

    </head>

    <body>
        <header>
            <nav>
                <ul class="navi">
                </ul>
            </nav>
        </header>
        <div class="titre">
            <br>
            <h1>la balise &#60;body&#62;</h1>
            <br>
            <p> 
                La balise &#60;body&#62; définit la partie principale du document, le body contient les informations qui seront affichées sur le site.
                Cette balise contient tous les contenus de L'HTML5 comme du texte, des images, des tableaux ...<br>
                <br>
                &#60;body&#62; est une balise de type block.

            </p>
            <p> background= URL pour l'image de fond. <br>
                bgcolor= définit la couleur du fond. <br>

            </p>

            <p>
                <br>
                <img src="image/balise%20body.PNG" alt="balise &#60;body&#62;"/>
            </p>
            <br>
            <h4><a href="index_corp.php" >Sommaire corps</a></h4>
            <br>
            <h4><a href="index.php" >Retour au sommaire</a></h4>
            <br>
        </div>
    </body>    
</html>