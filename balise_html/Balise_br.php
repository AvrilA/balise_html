<!DOCTYPE html>
<html lang="fr">

    <head>
        <link rel="icon" href="image/7BQx50y.ico" />
        <link rel="stylesheet" type="text/css" href="style_1.css"> 
        <meta charset="utf-8">
        <title>la balise &#60;br&#62;</title>

    </head>

    <body>
        <header>
            <nav>
                <ul class="navi">
                </ul>
            </nav>
        </header>
        <div class="titre">

            <h1>La balise &#60;br&#62; </h1>
            <br>

            <p> 
                La balise &#60;br&#62; crée un saut de ligne dans le texte. Il est utile lorsque les sauts de lignes sont importants (adresse ou poème).<br><br> &#60;br&#62; est une balise inline.

            </p> 
            <br>
            <br>
            <br>
            <p><img class="imagebr" src="image/balise%20br.png" alt="balise &#60;br&#62;"/></p>
            <br>
            <br>
            <br>
            <h4><a href="index_inline.php" >Sommaire inline</a></h4>
            <br>
            <h4><a href="index.php" >Retour au sommaire</a></h4>
            <br>
        </div>
    </body>    
</html>