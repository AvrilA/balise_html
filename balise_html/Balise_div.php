<!Doctype HTML>
<html lang="fr">
    <head>
        <link rel="icon" href="image/7BQx50y.ico" />
        <link rel="stylesheet" type="text/css" href="style_1.css">
        <meta charset="utf-8">
        <title>Présentation de la balise &#60;div&#62;</title>
    </head>    

    <body>
        <header>
            <nav>
                <ul class="navi">
                </ul>
            </nav>
        </header>
        <div class="titre">
            <br>
            <h1>La balise  &#60;div&#62;</h1>
            <br>
            <p>
                L'élément &#60;div&#62; (qui signifie division du document) est un conteneur générique qui permet d'organiser
                le contenu. Il peut être utilisé afin de grouper d'autres éléments pour leur appliquer un style
                (en utilisant les attributs class ou id) ou parce qu'ils partagent des attributs communs.<br />
                L'élément &#60;div&#62; doit uniquement être utilisé lorsqu'il n'existe aucun autre élément
                dont la sémantique permet de représenter le contenu <br/>(par exemple &#60;nav&#62;).
            </p>
            <br>
            <p>
                <img src="image/exemple-code-balise-div.png" alt="exemple de code pour la balise &#60;p&#62;">
            </p>
            <br>
            <p>
                <img src="image/exemple-balise-div.png" alt="exemple rendu de code pour la balise &#60;p&#62;">
            </p>
            <br>
            <h4><a href="index_block.php" >Sommaire block</a></h4>
            <br>
            <h4><a href="index.php" >Retour au sommaire</a></h4>

            <br>
        </div>
    </body>

</html>