<!DOCTYPE html>
<html lang="fr">

    <head>
        <link rel="icon" href="image/7BQx50y.ico" />
        <link rel="stylesheet" type="text/css" href="style_1.css"> 
        <meta charset="utf-8">
        <title>la balise &#60;form&#62;</title>

    </head>

    <body>
        <header>
            <nav>
                <ul class="navi">
                </ul>
            </nav>
        </header>
        <div class="titre">
            <br>
            <h1>la balise &#60;form&#62;</h1>
            <br>
            <p> 
                La balise &#60;form&#62; représente une section d'un document qui contient des contrôles interactifs qui permet à l'utilisateur d'envoyer des données à un seveur web.<br>
                &#60;form&#62; est une balise block.
            </p>
            <br>
            <p>L'attribut autocomplete définit un élément de formulaire pouvant être rempli automatiquement par le navigateur  </p>
            <br>
            <p>
                <img src="image/balise%20form.PNG" alt="balise &#60;form&#62;"/>
            </p>
            <br>
            <h4><a href="index_block.php" >Sommaire block</a></h4>
            <br>
            <h4><a href="index.php" >Retour au sommaire</a></h4>
        </div>
    </body>    
</html>