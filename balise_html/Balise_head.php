<!Doctype HTML>
<html lang="fr">
    <head>
        <link rel="icon" href="image/7BQx50y.ico" />
        <link rel="stylesheet" type="text/css" href="style_1.css">
        <meta charset="utf-8">
        <title>Présentation de la balise &#60;head&#62;</title>
    </head>    

    <body>
        <header>
            <nav>
                <ul class="navi">
                </ul>
            </nav>
        </header>
        <div class="titre">
            <br>
            <h1>La balise &#60;head&#62;</h1>
            <br>
            <p>La balise &#60;head&#62; contient tous les éléments de l'en-tête de votre document. C'est cette balise qui contiendra
                le titre de votre page mais elle pourra également accueillir vos scripts, styles, balises meta etc.
            </p>
            <br>
            <p>L'attribut dropzone définit une zone ou l'on peut faire des glisser-déposer de fichiers documents. </p>
            <br>
            <p>
                <img src="image/exemple-code-balise-head.png" alt="Exemple de code pour la balise head.">
                <img src="image/exemple-balise-head.png" alt="Rendu de code pour la balise head.">
            </p>
            <br>
            <p>La balise &#60;meta&#62; est ici utilisé afin de définir l'encodage employé dans la page. utf-8 permet notamment d'afficher
                correctement les accents et non pas d'afficher des caractères spéciaux à leur place.<br />
                La balise &#60;title&#62; permet d'indiquer le titre de la page dans l'onglet du navigateur.</p>
            <br>
            <h4><a href="index_corp.php" >Sommaire corps</a></h4>
            <br>
            <h4><a href="index.php" >Retour au sommaire</a></h4>
            <br>
        </div>
    </body>

</html>