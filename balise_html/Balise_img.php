<!Doctype HTML>
<html lang="fr">
    <head>
        <link rel="icon" href="image/7BQx50y.ico" />
        <link rel="stylesheet" type="text/css" href="style_1.css">
        <meta charset="utf-8">
        <title>Présentation de la balise &#60;img&#62;</title>
    </head>

    <body>
        <header>
            <nav>
                <ul class="navi">
                </ul>
            </nav>
        </header>
        <div class="titre">
            <br>
            <h1>La balise &#60;img&#62;</h1>
            <br>
            <p>
                La balise &#60;img&#62; est un élément de type inline permettant de faire apparaître une image dans un document.<br />
                Les éléments inline, à l'inverse des éléments Block, peuvent se placer les uns à côté des autres si la largeur de
                la page le permet.
            </p>
            <br>
            <p>
                Il est fortement conseillé d'utiliser des attributs lors de l'utilisation de cette balise afin de faciliter le référencement
                naturel.
            </p>
            <br>
            <h4> On peut utiliser les attributs :</h4>
            <br>
            <p>src : L'URL de l'image. Cet attribut est obligatoire pour l'élément &#60;img&#62;<br>
                alt : qui affiche un texte alternatif si l'image ne peut pas être afficher.
                - height : la hauteur intrinsèque de l'image exprimée en pixels.<br/>
                - width : La largeur intrinsèque de l'image exprimée en pixels.
            </p>
            <br>

            <img class="img-code" src="image/exemple-code-balise-img.png" alt="exemple de code pour la balise &#60;img&#62;">
            <img class="img-jo" src="image/jeuxolympiques.jpg" alt="exemple rendu de code pour la balise &#60;img &#62;">
            <br>
            <h4><a href="index_inline.php" >Sommaire inline</a></h4>
            <br>
            <h4><a href="index.php" >Retour au sommaire</a></h4>
            <br>
        </div>
    </body>

</html>