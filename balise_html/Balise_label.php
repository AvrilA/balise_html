<!DOCTYPE html>
<html lang="fr">

    <head>
        <link rel="icon" href="image/7BQx50y.ico" />
        <link rel="stylesheet" type="text/css" href="style_1.css"> 
        <meta charset="utf-8">
        <title>La balise &#60;label&#62;</title>

    </head>

    <body>
        <header>
            <nav>
                <ul class="navi">
                </ul>
            </nav>
        </header>
        <div class="titre">
            <br>
            <h1>La balise &#60;label&#62;</h1>
            <br>
            <p> 
                La balise &#60;label&#62; sert a définir la légende pour un objet d'une interface.
                <br>
                &#60;label&#62; est une balise du groupement de balises formulaires.
            </p>
            <br>
            <p>
                <img src="image/Balise_Label2.PNG" alt="balise &#60;label&#62;"/>
            </p>
            <br>
            <p><a href="index_formulaires.php" >Sommaire Formulaires</a></p>
            <br>
            <p><a href="index.php" >Retour au sommaire</a></p>
        </div>
    </body>    
</html>