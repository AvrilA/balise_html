<!Doctype HTML>
<html lang="fr">
    <head>
        <link rel="icon" href="image/7BQx50y.ico" />
        <link rel="stylesheet" type="text/css" href="style_1.css">
        <meta charset="utf-8">
        <title>Présentation de la balise &#60;p&#62;</title>
    </head>

    <body>
        <header>
            <nav>
                <ul class="navi">
                </ul>
            </nav>
        </header>
        <div class="titre">
            <br>
            <h1>La balise &#60;p&#62;</h1>
            <br>
            <p>La balise &#60;p&#62; est un élément de type Block. C'est à dire que ces éléments se positionne les uns sous les autres
                et provoque un retour à la ligne une fois fermée.
            </p>
            <br>
            <p>
                Cette balise est utilisée afin de délimiter un paragraphe de texte à l'aide de la balise ouvrante et fermante &#60;p&#62;et &#60;/p&#62;.
            </p>
            <br>
            <p>
                <img src="image/exemple-code-balise-p.png" alt="exemple de code pour la balise &#60;p&#62;">
            </p>
            <br>
            <p>
                <img src="image/exemple-balise-p.png" alt="exemple rendu de code pour la balise &#60;p&#62;">
            </p>
            <br>
            <h4><a href="index_block.php" >Sommaire block</a></h4>
            <br>
            <h4><a href="index.php" >Retour au sommaire</a></h4>
            <br>
        </div>
    </body>
</html>