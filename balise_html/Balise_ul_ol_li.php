<!DOCTYPE html>
<html lang="fr">

    <head>
        <link rel="icon" href="image/7BQx50y.ico" />
        <link rel="stylesheet" type="text/css" href="style_1.css">
        <meta charset="utf-8">
        <title>les balises &#60;ul&#62; &#60;ol&#62; &#60;li&#62;</title>

    </head>

    <body>
        <header>
            <nav>
                <ul class="navi">
                </ul>
            </nav>
        </header>
        <div class="titre">
            <br>
            <h1>les balises &#60;ul&#62; &#60;ol&#62; &#60;li&#62;</h1>
            <br>
            <p> 
                Les balises &#60;ul&#62; &#60;ol&#62; &#60;li&#62; permettent la création de listes ordonnées avec &#60;ol&#62; et non-ordonées avec &#60;ul&#62; et la balise &#60;li&#62; 
                permet de délimiter chacun des élements de la liste.<br>
                Ce sont des balises de type Block.</p>
            <br>
            <p>
                <img src="image/balise%20ul%20ol.png" alt="balise &#60;ul&#62; &#60;ol&#62; &#60;li&#62;"/>
            </p>
            <br>
            <h4><a href="index_block.php" >Sommaire block</a></h4>
            <br>
            <h4><a href="index.php" >Retour au sommaire</a></h4>
        </div>
    </body>    
</html>