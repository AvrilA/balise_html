<!DOCTYPE html>
<html lang="fr">

    <head>
        <link rel="icon" href="image/7BQx50y.ico" />
        <link rel="stylesheet" type="text/css" href="style_1.css"> 
        <meta charset="utf-8">
        <title>la balise &#60;input&#62;</title>

    </head>

    <body>
        <header>
            <nav>
                <ul class="navi">
                </ul>
            </nav>
        </header>
        <div class="titre">
            <br>
            <h1>la balise &#60;input&#62;</h1>
            <br>
            <p> 
                La balise &#60;input&#62; est utilisé pour créer une intéraction dans un formulaire afin de
                permettre à l'utilisateur de saisir des données. C'est une balise orpheline, c'est à dire qu'elle à une
                balise ouvrante mais pas de balise fermante. Le comportement de l'élément &#60;input&#62;
                dépend de la valeur indiqué dans son attribut "type".
            </p>
            <br>
            <p>
                <img src="image/balise-input.png" alt="balise &#60;input&#62;"/>
            </p>
            <br>
            <p>
                La valeur initiale du type est "text" afin de rédiger un texte mais vous pouvez donc également
                utiliser:</p>
            <ul>
                <li>- number : afin d'écrire un chiffre ou d'en sélectionner un avec les flêches.</li>
                <li>- color: afin d'ouvrir la palette de couleur de votre ordinateur et vous permettre de choisir une nuance.</li>
                <li>- checkbox : afin de créer un bouton qu'il vous sera possible de cocher. Il est souvent permis d'en cocher plusieurs</li>
                <li>- radio: afin de créer un bouton radio qu'il vous sera possible de cocher avec un petit rond. Il est souvent permis de n'en cocher qu'un seul. </li>
                <li>- date: vous permez d'incrire une donnée au format date avec jour, mois et année.</li>
                <li>- file : fait apparaitre le bouton parcourir afin de pouvoir sélectionner l'un de vos fichiers.</li>
                <li>- month : afin de précisez un mois spécifique.</li>
                <li>- password : afin de vous permettre de rentrer votre mot de passe sans qu'il soit lisible (affiche des petits ronds pour chaque caractètre entré.)</li>
                <li>- range : fait apparaître une rêglette que l'on peut faire bouger de droite à gouche souvent utiliser pour désigner le niveau de quelque chose.</li>
                <li>- time: permet d'afficher une donnée sous le format heure et minutes.</li>
                <li>- reset : fait apparaître un bouton permettant de réinitialiser le contenu des éléments du formulaire et remettre leurs valeurs par défaut.</li>
                <li>- search : Un champ de texte sur une ligne permettant de saisir une recherche. Les sauts de ligne sont automatiquement supprimées de la saisie.</li>
                <li>- url : un champ qui permet d'éditer une url</li>
            </ul>
            <br>
            <p>
                Il éxiste différents attributs pour la balise &#60;input&#62; comme le type mais aussi bien d'autres.<br>
                - accept : Si la valeur de l'attribut type est file, cet attribut indiquera les types de fichier acceptés par le
                serveur. La valeur de cet attribut est une liste de format, séparés par des virgules. Un descripteur de format peut être
                une extension de fichier comme les .doc, .png, etc. des documents de type audio, video ou image.
                - autocomplete : Cet attribut permet d'indiquer si la valeur saisie doit automatique être complétée par le navigateur. Il éxiste
                différente valeur pour cet attribut:
                <br>
            <ul>
                <li>off : aucune auto-complétion n'est effectuée par le navigateur. L'utilisateur doit saisir le champ à chaque utilisation.</li>
                <li>on : le navigateur peut compléter automatiquement la valeur saisie par l'utilisateur en fonction des valeurs précédemment saisies.</li>
                <li>name : un nom complet</li>
                <li>honorific-prefix : un préfixe ou un titre comme M., Mme., etc.</li>
                <li>given-name : un prénom</li>
                <li>additional-name : un deuxième prénom</li>
                <li>family-name : un nom de famille</li>
                <li>nickname : un surnom ou un pseudonyme</li>
                <li>email : une adresse électronique</li>
                <li>username : un nom d'utilisateur</li>
                <li>new-password : un nouveau mot de passe (par exemple lorsqu'on crée un compte ou qu'on change un mot de passe).</li>
                <li>current-password : un mot de passe actuel</li>
                <li>organization-title : un poste dans l'entreprise ex: PDG, etc.</li>
                <li>organization : un nom pour une entreprise ou une organisation</li>
                <li>street-address : le numéro d'une adresse</li>
                <li>address-line1, address-line2, address-line3, address-level4, address-level3, address-level2, address-level1 : les différents détails d'une 'adresse</li>
                <li>country : un pays</li>
                <li>country-name : un nom de pays</li>
                <li>postal-code : un code postal</li>
                <li>cc-name : un nom complet, tel qu'indiqué sur un moyen de paiement</li>
                <li>cc-given-name : un nom usuel, tel qu'indiqué sur un moyen de paiement</li>
                <li>cc-additional-name : un nom complémentaire, tel qu'indiqué sur un moyen de paiement</li>
                <li>cc-family-name : un nom de famille, tel qu'indiqué sur un moyen de paiement</li>
                <li>cc-number : un code identifiant le moyen de paiement (le numéro d'une carte de crédit par exemple)</li>
                <li>cc-exp : la date d'expiration du moyen de paiement</li>
                <li>cc-exp-month : le mois d'expiration du moyen de paiement</li>
                <li>cc-exp-year : l'année d'expiration du moyen de paiement</li>
                <li>cc-csc : le code de sécurité du moyen de paiement</li>
                <li>cc-type : le type de moyen de paiement (par exemple « Visa »).</li>
                <li>transaction-currency : la devise dans laquelle est effectuée la transaction</li>
                <li>transaction-amount : le montant de la transaction</li>
                <li>language : la langue privilégiée</li>
                <li>bday : une date d'anniversaire</li>
                <li>bday-day : le jour d'une date d'anniversaire</li>
                <li>bday-month : le mois d'une date d'anniversaire</li>
                <li>bday-year : l'année d'une date d'anniversaire</li>
                <li>sex : le genre d'une personne</li>
                <li>tel : un numéro de téléphone complet avec l'indicateur du pays</li>
                <li>url : l'URL d'une page web correspondant à l'entreprise, la personne, l'adresse ou le 
                    contact auquel les informations saisies font référence></li>
                <li>photo : une photographie, une icône ou une image qui correspond à l'entreprise, la personne, 
                    l'adresse ou le contact auquel les informations saisies font référence.</li>
            </ul>
            <br>
            Cependant il faut faire attention lorsqu'utiliser dans différents navigateurs car par exemple la plupart des navigateurs modernes
            comme Chrome, Firefox, IE, et Google) demanderont à l'utilisateur si il souhaite enregistrer ses informations de connexion et compléter
            automatique les informations de connexion la prochaine fois que l'utilisateur ira sur cette page.

            - autofocus: Cet attribut booléen permet d'indiquer que l'élément doit recevoir le focus au chargement de la page. 
            Cet attribut ne peut pas être appliqué si type vaut hidden.

            - capture : Lorsque l'attribut type vaut file, la présence de cet attribut booléen indique qu'il faut, si possible, utiliser un périphérique
            de capture de l'appareil plutôt qu'un explorateur de fichier.

            - checked : Lorsque l'attribut type vaut radio ou checkbox, la présence de cet attribut booléen indique que le contrôle doit être sélectionné par défaut. 
            Pour les autres types, cet attribut est ignoré.

            -height :Si la valeur de l'attribut type vaut image, cet attribut définit la hauteur de l'image affichée sur le bouton.

            - inputmode : Une indication destinée au navigateur pour le clavier qui doit être affiché. Cet attribut s'applique uniquement lorsque l'attribut type vaut text, password, 
            email, ou url. Les valeurs possibles pour cet attribut sont :
            - none : aucun clavier virtuel ne devrait être affiché.
            - text : un clavier pour saisir du texte dans la langue de l'utilisateur
            - decimal : un clavier pour saisir des nombres décimaux.
            - numeric : un clavier numérique qui contient des touches pour les chiffres 0 à 9, une touche pour le caractères qui permet de séparer les milliers et un caractère pour indiquer les nombres négatifs. 
            Ce clavier est destiné à la saisie de codes numériques (par exemple le numéro d'une carte de crédit), pour saisir des nombres, on privilégiera &#60;input type="number"&#62;.
            - tel : un clavier téléphonique, qui contient une astérisque et le dièse. Si possible, on utilisera &#60;input type="tel"&#62; plutôt que cette valeur d'attribut.
            - email : un clavier destiné à la saisie d'une adresse électronique. Si possible, on utilisera &#60;input type="email"&#62; plutôt que cette valeur d'attribut.
            - url : un clavier destiné à la saisie d'URL. Si possible, on privilégiera &#60;input type="url"&#62; à cet attribut.

            - placeholder : Une indication, destinée à l'utilisateur, pour ce qui peut être saisi dans le contrôle. Le texte de cet attribut ne doit pas contenir de saut de ligne. 

            - size : permet d'agrandir le champ de saisie.

            - max-length : permet de limiter le nombre de caractères que l'on peut saisir.
            <br>
            <h4><a href="index_block.php" >Sommaire block</a></h4>
            <br>
            <h4><a href="index.php" >Retour au sommaire</a></h4>
        </div>
    </body>    
</html>