<!DOCTYPE html>
<html lang="fr">

    <head>
        <link rel="icon" href="image/7BQx50y.ico" />
        <link rel="stylesheet" type="text/css" href="style_1.css">
        <meta charset="utf-8">
        <title>les balises &#60;select&#62;</title>

    </head>

    <body>
        <header>
            <nav>
                <ul class="navi">
                </ul>
            </nav>
        </header>
        <div class="titre">
            <br>
            <h1>la balise &#60;select&#62;</h1>
            <br>
            <p> 
                La balise &#60;SELECT&#62; permet de créer une liste déroulante ou à choix multiples. C'est l'un des différents contrôles utilisables au sein d'un formulaire FORM pour rapatrier des informations
                <br>
                <br>
            <p> - disabled :	rend le contrôle passif, non modifiable.<br>
                - multiple :	autorise la sélection multiple d'éléments de la liste.<br>
                - size :	définit le nombre de valeurs visibles.<br></p>
            <p>
                <br>
                <img src="image/select_code.PNG" alt="balise &#60;select&#62;"/>
                <img src="image/select_img.png" alt="balise &#60;select&#62;"/>

            </p>
            <br>
            <h4><a href="index_formulaires.php" >Sommaire block</a></h4>
            <br>
            <h4><a href="index.php" >Retour au sommaire</a></h4>
        </div>
    </body>    
</html>