<!DOCTYPE html>
<html lang="fr">

    <head>
        <link rel="icon" href="image/7BQx50y.ico" />
        <link rel="stylesheet" type="text/css" href="style_1.css">
        <meta charset="utf-8">
        <title>Index des balises HTML</title>

    </head>

    <body>
        <?php include('header.inc.php'); ?>


        <div class="titre">
            <br>
            <br>
            <br>
            <br>
            <h1> Index des balises HTML</h1>
            <br>
            <h4>Liste de définitions des balises référencées sur le site. Ici vous pourrez trouvez diverses définitions des balises les plus utilisées en
                HTML. Lisez chaque définition à votre guise ou naviguer dans le menu ci-dessus afin de voir dans quelle catégorie sont réparties chaque balises.
            </h4>
            <br/>
            <ul class="def">

                <li class="class"><a class="defini" href="balise_head.php" > &#60;head&#62;</a> </li>
                <br>
                <li class="class"><a class="defini" href="Balise_body.php" >&#60;body&#62;</a></li>
                <br>
                <li class="class"><a class="defini" href="Balise_Footer.php" >&#60;footer&#62;</a></li>
                <br>
                <li class="class"><a class="defini" href="Balise_H.php" >&#60;H&#62;</a></li>
                <br>
                <li class="class"><a class="defini" href="Balise_p.php" >&#60;P&#62;</a></li>
                <br>
                <li class="class"><a class="defini" href="Balise_img.php" >&#60;img&#62;</a></li>
                <br>
                <li class="class"><a class="defini" href="Balise_ul_ol_li.php" >&#60;ul/ol/li&#62;</a></li>
                <br>
                <li class="class"><a class="defini" href="Balise_Textarea.php" >&#60;textarea&#62;</a></li>
                <br>
                <li class="class"><a class="defini" href="Balise_A.php" >&#60;A&#62;</a></li>
                <br>
                <li class="class"><a class="defini" href="Balise_div.php" >&#60;div&#62;</a></li>
                <br>
                <li class="class"><a class="defini" href="Balise_form.php" >&#60;form&#62;</a></li>
                <br>
                <li class="class"><a class="defini" href="Balise_br.php" >&#60;br&#62;</a></li>
                <br>
                 <li class="class"><a class="defini" href="balise_input.php" >&#60;input&#62;</a></li>
                <br>
                <li class="class"><a class="defini" href="balise_select.php" >&#60;select&#62;</a></li>
                <br>
                <li class="class"><a class="defini" href="Balise_label.php" >&#60;label&#62;</a></li>

            </ul>
            <br>
            <br>
            <br>
            <br>
            <br>
            <h4>Site réalisé par Adrien, François et Laëtitia.</h4>
        </div>
    </body>

</html>