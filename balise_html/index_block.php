<!DOCTYPE html>
<html lang="fr">

    <head>
        <link rel="icon" href="image/7BQx50y.ico" />
        <link rel="stylesheet" type="text/css" href="style_1.css">
        <meta charset="utf-8">
        <title>Index des balises HTML</title>
    </head>

    <body>   
        <?php include('header.inc.php'); ?>


        <div class="titre">
            <br>
            <br>
            <br>
            <br>
            <br>
            <h1> Les balises block</h1>
            <br/>
            <h4>Les balises block souvent utilisées sont listées ci-dessous. Les balises Block 
                font que chaque élèment sont placés les uns sous les autres.</h4>
            <br/>
            <ul>
                <li class="class"><a href="Balise_p.php" >&#60;P&#62;</a></li>
                <br>
                <li class="class"><a href="Balise_ul_ol_li.php" >&#60;ul/ol/li&#62;</a></li>
                <br>
                <li class="class"><a href="Balise_Textarea.php" >&#60;textarea&#62;</a></li>
                <br>
                <li class="class"><a href="Balise_div.php" >&#60;div&#62;</a></li>
                <br>
                <li class="class"><a href="Balise_form.php" >&#60;form&#62;</a></li>
                <br>
                <li class="class"><a href="Balise_H.php" >&#60;H&#62;</a></li>

                <br>
                <br>
                <br>
                <br>
            </ul>
            <h4><a href="index.php" >Retour au sommaire</a></h4>
            <br>
        </div>
    </body>