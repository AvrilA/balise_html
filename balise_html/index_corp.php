<!DOCTYPE html>
<html lang="fr">

    <head>
        <link rel="icon" href="image/7BQx50y.ico" />
        <link rel="stylesheet" type="text/css" href="style_1.css">
        <meta charset="utf-8">
        <title>Index des balises HTML</title>
    </head>

    <body>   
        <?php include('header.inc.php'); ?>


        <div class="titre">
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <h1> Les balises corps</h1>
            <br/>
            <h4>Ici sont listés les balises utilisées afin de délimiter le corps du texte. 
                Les deux premières étant obligatoires si l'ont veux undocument HTML fonctionnel.
            </h4>
            <br/>
            <ul>
                <li class="class"><a href="balise_head.php" > &#60;head&#62;</a> </li>
                <br/>
                <li class="class"><a href="Balise_body.php" >&#60;body&#62;</a></li>
                <br/>
                <li class="class"><a href="Balise_Footer.php" >&#60;footer&#62;</a></li>
                <br/>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
            </ul>
            <h4><a href="index.php" >Retour au sommaire</a></h4>
            <br>
        </div>
    </body>